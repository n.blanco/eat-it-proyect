package niyuby.app.com.androideatit.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import niyuby.app.com.androideatit.R;
import niyuby.app.com.androideatit.model.User;

public class SignUpActivity extends AppCompatActivity {
    EditText editText1,editText2,editText3,editText4;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        button=(Button)findViewById(R.id.btn_signUp);
        editText1=(MaterialEditText)findViewById(R.id.edittxt_phone);
        editText2=(MaterialEditText)findViewById(R.id.edittxt_name);
        editText3=(MaterialEditText)findViewById(R.id.edittxt_pass);
        editText4=(MaterialEditText)findViewById(R.id.edittxt_secure_code);
        //Comenzando con Firebase

        final FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
        final DatabaseReference databaseReference=firebaseDatabase.getReference("User");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressDialog=new ProgressDialog(SignUpActivity.this);
                progressDialog.setMessage("Please waiting...");
                progressDialog.show();
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Verficando que existe el usuario...
                        if(dataSnapshot.child(editText1.getText().toString()).exists()){
                            //Obteniendo la informacion...
                            progressDialog.dismiss();
                            Toast.makeText(SignUpActivity.this,"Phone number already register",Toast.LENGTH_LONG).show();


                        }else {
                            progressDialog.dismiss();
                            User user=new User(editText1.getText().toString(),editText2.getText().toString(),editText3.getText().toString(),
                                    editText4.getText().toString());
                            databaseReference.child(editText1.getText().toString()).setValue(user);
                            Toast.makeText(SignUpActivity.this,"Sign up successfully",Toast.LENGTH_LONG).show();
                             finish();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }
}
