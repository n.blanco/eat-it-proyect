package niyuby.app.com.androideatit.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

import dmax.dialog.SpotsDialog;
import niyuby.app.com.androideatit.R;
import niyuby.app.com.androideatit.common.Common;
import niyuby.app.com.androideatit.database.Database;
import niyuby.app.com.androideatit.interfacemodel.ItemClickListener;
import niyuby.app.com.androideatit.model.Category;
import niyuby.app.com.androideatit.model.User;
import niyuby.app.com.androideatit.viewholder.MenuViewHolder;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
     FirebaseDatabase firebaseDatabase;
     DatabaseReference databaseReference;
     TextView txt_fullname;
     RecyclerView recyclerView;
     RecyclerView.LayoutManager layoutManager;
     FirebaseRecyclerAdapter<Category,MenuViewHolder>adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Menu");
        setSupportActionBar(toolbar);

        //Empezando con Firebase
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference("Category");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent  cartIntent=new Intent(HomeActivity.this,CartActivity.class);
                startActivity(cartIntent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Nombre completo para el usuario
        View headerviw=navigationView.getHeaderView(0);
        txt_fullname=(TextView)headerviw.findViewById(R.id.txt_fullname);
        txt_fullname.setText(Common.commonUser.getName());

        //Cargando el menu
        recyclerView=(RecyclerView)findViewById(R.id.recycler_home);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(HomeActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        
        loadMenu();

    }

    private void loadMenu() {
      adapter=new FirebaseRecyclerAdapter<Category, MenuViewHolder>(Category.class,
                R.layout.menu_item,MenuViewHolder.class,databaseReference) {
            @Override
            protected void populateViewHolder(MenuViewHolder viewHolder, Category model, int position) {
            viewHolder.textView.setText(model.getName());
                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.imageView);

                final Category category=model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                       // Toast.makeText(HomeActivity.this,""+category.getName(),Toast.LENGTH_LONG).show();
                        //Toast.makeText(HomeActivity.this,""+category.getImage(),Toast.LENGTH_LONG).show();

                        // Obteniendo la comida de acuerdo a la categoria
                        Intent foods=new Intent(HomeActivity.this,FoodListActivity.class);
                        foods.putExtra("CategoryId",adapter.getRef(position).getKey());
                        startActivity(foods);
                    }
                });
            }
        };
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

          if (id == R.id.nav_cart) {
              Intent  cartIntent=new Intent(HomeActivity.this,CartActivity.class);
              startActivity(cartIntent);
        }else if (id == R.id.nav_change_pass) {
              showChangePass();

        }else if (id == R.id.nav_logout) {
              Intent  signInIntent=new Intent(HomeActivity.this,SignInActivity.class);
              signInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
              startActivity(signInIntent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showChangePass() {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
        alertDialog.setTitle("Change Password...").setMessage("Please fill all information");

        LayoutInflater inflater=this.getLayoutInflater();
        View change_view=inflater.inflate(R.layout.change_pass_layout,null);
        alertDialog.setView(change_view);

        final MaterialEditText editText2=(MaterialEditText)change_view.findViewById(R.id.edittxt_pass);
        final MaterialEditText editText3=(MaterialEditText)change_view.findViewById(R.id.edittxt_new_pass);
        final MaterialEditText editText4=(MaterialEditText)change_view.findViewById(R.id.edittxt_repeat_pass);
        alertDialog.setPositiveButton("CHANGE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
              final android.app.AlertDialog alertDialog1=new SpotsDialog(HomeActivity.this);
                alertDialog1.show();
                if (editText2.getText().toString().equals(Common.commonUser.getPassword())){
                    if (editText3.getText().toString().equals(editText4.getText().toString())){
                        Map<String,Object> updatePass=new HashMap<>();
                        updatePass.put("Password",editText3.getText().toString());
                        DatabaseReference database=FirebaseDatabase.getInstance().getReference("User");
                        database.child(Common.commonUser.getPhone()).updateChildren(updatePass).addOnCompleteListener(
                                new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        alertDialog1.dismiss();

                                        Toast.makeText(HomeActivity.this,"Password was update",Toast.LENGTH_LONG).show();
                                        Intent  signInIntent=new Intent(HomeActivity.this,SignInActivity.class);
                                        signInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(signInIntent);
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(HomeActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();

                            }
                        });


                    } else{
                        alertDialog1.dismiss();
                        Toast.makeText(HomeActivity.this,"New password doesn't match",Toast.LENGTH_LONG).show();

                    }

                }else {
                    alertDialog1.dismiss();
                    Toast.makeText(HomeActivity.this,"Wrong old password!",Toast.LENGTH_LONG).show();
                }
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }
}
