package niyuby.app.com.androideatit.activities;

import android.media.Image;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.CollapsibleActionView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import niyuby.app.com.androideatit.R;
import niyuby.app.com.androideatit.database.Database;
import niyuby.app.com.androideatit.model.Foods;
import niyuby.app.com.androideatit.model.Order;

public class FoodDetailActivity extends AppCompatActivity {
    TextView foodName,foodPrice,foodDescription;
    ImageView imgFood;
    CollapsingToolbarLayout collapsingToolbarLayout;
    FloatingActionButton actionButton;
    ElegantNumberButton buttonNumber;

    String foodId="";
    FirebaseDatabase firebaseDatabase;
    DatabaseReference foods;
    Foods currentFoods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);

        //Firebase
        firebaseDatabase=FirebaseDatabase.getInstance();
        foods=firebaseDatabase.getReference("Foods");

        // Vista
         buttonNumber= (ElegantNumberButton)findViewById(R.id.number_button);
         actionButton=(FloatingActionButton)findViewById(R.id.floating_cart);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Database(getBaseContext()).addToCart(new Order(foodId,
                        currentFoods.getName(),
                        buttonNumber.getNumber(),
                        currentFoods.getPrice(),
                        currentFoods.getDiscount()



                ));
                Toast.makeText(FoodDetailActivity.this,"Add to Cart",Toast.LENGTH_LONG).show();
            }
        });

        foodName=(TextView) findViewById(R.id.food_name);
        foodPrice=(TextView)findViewById(R.id.food_price);
        foodDescription=(TextView)findViewById(R.id.txt_foodDescription);

        imgFood=(ImageView)findViewById(R.id.img_food);

        collapsingToolbarLayout=(CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);

        //Obteniendo el intent
        if(getIntent()!=null){
            foodId =getIntent().getStringExtra("FoodId");
            if (!foodId.isEmpty()&&foodId!=null){
                loadFood(foodId);
            }

        }



    }

    private void loadFood(final String foodId) {
        foods.child(foodId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                currentFoods=dataSnapshot.getValue(Foods.class);

                //Imagen
                Picasso.with(getBaseContext()).load(currentFoods.getImage()).into(imgFood);

                collapsingToolbarLayout.setTitle(currentFoods.getName());

                foodPrice.setText(currentFoods.getPrice());
                foodName.setText(currentFoods.getName());
                foodDescription.setText(currentFoods.getDescription());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
