package niyuby.app.com.androideatit.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import niyuby.app.com.androideatit.R;
import niyuby.app.com.androideatit.common.Common;
import niyuby.app.com.androideatit.config.Config;
import niyuby.app.com.androideatit.database.Database;
import niyuby.app.com.androideatit.helper.RecyclerItemTouchHelper;
import niyuby.app.com.androideatit.interfacemodel.RecyclerItemTouchHelperListener;
import niyuby.app.com.androideatit.model.Order;
import niyuby.app.com.androideatit.model.Request;
import niyuby.app.com.androideatit.viewholder.CartAdapter;
import niyuby.app.com.androideatit.viewholder.CartViewHolder;

public class CartActivity extends AppCompatActivity implements RecyclerItemTouchHelperListener {
    public static final int PAYPAL_REQUEST_CODE = 7171;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference; //request
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    TextView txt_price;
    Button btn_placeOrder;
    List<Order> cart= new ArrayList<>();
    CartAdapter adapter;
    RelativeLayout rooot_layout;
    private static PayPalConfiguration configuration=new PayPalConfiguration().
            environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Config.PAYPAL_CLIENT_ID);
    String address,comment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        // Configurando Paypal
        Intent intent=new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,configuration);
        startService(intent);

        //Empezando con Firebase
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference("Requests");

        //Cargando el menu
        recyclerView=(RecyclerView)findViewById(R.id.recycler_cart);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(CartActivity.this);
        recyclerView.setLayoutManager(layoutManager);

        //Borrando las ordenes
         ItemTouchHelper.SimpleCallback simpleCallback= new RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT,CartActivity.this);
         new ItemTouchHelper(simpleCallback).attachToRecyclerView(recyclerView);


        txt_price=(TextView)findViewById(R.id.txt_total);
        btn_placeOrder=(Button)findViewById(R.id.btn_place_order);
        btn_placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               showAlertDialog();
            }
        });
        rooot_layout=(RelativeLayout)findViewById(R.id.layout_root);



        loadListFood();
    }
    public void initPayPal() {

        configuration = new PayPalConfiguration()
                .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                .clientId(getResources().getString(Integer.parseInt(Config.PAYPAL_CLIENT_ID)))
                .merchantName("BELxxxxxx")
                .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
                .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, configuration);
        startService(intent);
    }
    private void showAlertDialog() {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(CartActivity.this);
        alertDialog.setTitle("One more step!").setMessage("Enter your address: ");

        LayoutInflater inflater=this.getLayoutInflater();
        View change_view=inflater.inflate(R.layout.order_address_comment_layout,null);
        alertDialog.setView(change_view);
        final MaterialEditText editTextAddress=(MaterialEditText)change_view.findViewById(R.id.edittxt_address);
        final MaterialEditText editTextComment=(MaterialEditText)change_view.findViewById(R.id.edittxt_comment);
        alertDialog.setIcon(R.drawable.ic_shopping_cart_black2_24dp);

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Paypal configuracion
                //Obteniendo el address y common
                address=editTextAddress.getText().toString();
                comment=editTextComment.getText().toString();

                String formatAmount=txt_price.getText().toString().replace("$","").
                        replace(",","");
                PayPalPayment payPalPayment=new PayPalPayment(new BigDecimal(formatAmount),
                        "USD",
                       "Eat It App Order" ,
                        PayPalPayment.PAYMENT_INTENT_SALE);

                Intent intent=new Intent(getApplicationContext(),PaymentActivity.class);
                intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,configuration);
                intent.putExtra(PaymentActivity.EXTRA_PAYMENT,payPalPayment);
                startActivityForResult(intent,PAYPAL_REQUEST_CODE);


            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
         alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       if (requestCode==PAYPAL_REQUEST_CODE){
           if (resultCode==RESULT_OK){
               PaymentConfirmation confirmation=data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
               if(confirmation!=null){
                   try {
                     String paymentDetail=confirmation.toJSONObject().toString(4);
                       JSONObject jsonObject=new JSONObject(paymentDetail);
                         Request request= new Request(
                        Common.commonUser.getPhone(),
                        Common.commonUser.getName(),
                        address,
                         txt_price.getText().toString(),
                        "0",
                        comment,
                        jsonObject.getJSONObject("response").getString("state"),
                        cart
                );
                // Actualizando para Firebase
                databaseReference.child(String.valueOf(System.currentTimeMillis())).setValue(request);
                new Database(getBaseContext()).cleanCart();
                Toast.makeText(CartActivity.this,"Thank you, order place",Toast.LENGTH_LONG).show();
                finish();
                   }catch (JSONException e){
                       e.printStackTrace();
                   }
               }
           }else if (resultCode== Activity.RESULT_CANCELED)
               Toast.makeText(CartActivity.this,"Payment cancel",Toast.LENGTH_LONG).show();

           else if (resultCode==PaymentActivity.RESULT_EXTRAS_INVALID)
               Toast.makeText(CartActivity.this,"Invalid payment",Toast.LENGTH_LONG).show();
       }
    }

    private void loadListFood() {
        cart=new Database(this).getCarts(Common.commonUser.getPhone());
        adapter=new CartAdapter(cart,this);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        // Calculando el precio total

        int total=0;

        for(Order order:cart)
            total+=(Integer.parseInt(order.getPrice()))*(Integer.parseInt(order.getQuantity()));
        Locale locale=new Locale("en","US");
        NumberFormat numberFormat=NumberFormat.getCurrencyInstance(locale);
        txt_price.setText(numberFormat.format(total));
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if(viewHolder instanceof CartViewHolder){
            String name=((CartAdapter)recyclerView.getAdapter()).getItem(viewHolder.getAdapterPosition()).getProductName();
        final Order deteleItem=((CartAdapter)recyclerView.getAdapter()).getItem(viewHolder.getAdapterPosition());
        final int deleteIndex=viewHolder.getAdapterPosition();

        adapter.removeItem(deleteIndex);
       new Database(getBaseContext()).removeFromCart(deteleItem.getProductId());
                int total=0;
                List<Order> orders= new Database(getBaseContext()).getCarts(Common.commonUser.getPhone());
                for (Order item:orders)
                    total+=(Integer.parseInt(item.getPrice()))*(Integer.parseInt(item.getQuantity()));

            Locale  locale=new Locale("en","US");
            NumberFormat numberFormat=NumberFormat.getCurrencyInstance(locale);
             txt_price.setText(numberFormat.format(total));

             //snackBar
            Snackbar snackbar=Snackbar.make(rooot_layout,name+" removed from cart!",Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                 adapter.restoreItem(deteleItem,deleteIndex);
                 new Database(getBaseContext()).addToCart(deteleItem);
                    int total=0;

                    for(Order order:cart)
                        total+=(Integer.parseInt(order.getPrice()))*(Integer.parseInt(order.getQuantity()));
                    Locale locale=new Locale("en","US");
                    NumberFormat numberFormat=NumberFormat.getCurrencyInstance(locale);
                    txt_price.setText(numberFormat.format(total));
                }
            });
            snackbar.setActionTextColor(Color.GREEN);
            snackbar.show();
        }
    }
}
