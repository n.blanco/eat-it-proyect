package niyuby.app.com.androideatit.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import niyuby.app.com.androideatit.R;
import niyuby.app.com.androideatit.interfacemodel.ItemClickListener;

public class CartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public TextView txt_cart_name,txt_price;
    public ImageView img_count;
    private ItemClickListener itemClickListener;

    public RelativeLayout view_background;
    public LinearLayout view_foreground;

    public void setTxt_cart_name(TextView txt_cart_name) {
        this.txt_cart_name = txt_cart_name;
    }

    public CartViewHolder(View itemView) {
        super(itemView);
        txt_cart_name=(TextView)itemView.findViewById(R.id.txt_cart_item_name);
        txt_price=(TextView)itemView.findViewById(R.id.txt_cart_item_price);
        img_count=(ImageView)itemView.findViewById(R.id.img_cart_item_count);
        view_background=(RelativeLayout)itemView.findViewById(R.id.view_background);
        view_foreground=(LinearLayout)itemView.findViewById(R.id.view_forewround);
    }

    @Override
    public void onClick(View view) {

    }
}
