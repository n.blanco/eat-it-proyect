package niyuby.app.com.androideatit.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import niyuby.app.com.androideatit.R;
import niyuby.app.com.androideatit.activities.CartActivity;
import niyuby.app.com.androideatit.common.Common;
import niyuby.app.com.androideatit.database.Database;
import niyuby.app.com.androideatit.interfacemodel.ItemClickListener;
import niyuby.app.com.androideatit.model.Order;

public class CartAdapter extends RecyclerView.Adapter<CartViewHolder>{
    private List<Order> listData=new ArrayList<>();
    private Context context;
    private CartActivity cart;

    public CartAdapter(List<Order> listData, CartActivity context) {
        this.listData = listData;
        this.cart = context;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(cart);
        View view=inflater.inflate(R.layout.cart_layout,parent,false);
        return new CartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
       Locale  locale=new Locale("en","US");
       NumberFormat numberFormat=NumberFormat.getCurrencyInstance(locale);
       int price=(Integer.parseInt(listData.get(position).getPrice()))*(Integer.parseInt(listData.get(position).getQuantity()));
        /*Order order=listData.get(position);
        new Database(cart).updateCart(order);
        int total=0;
        List<Order> orders=new Database(cart).getCarts(Common.commonUser.getPhone());
        for(Order item:orders)

            total+=(Integer.parseInt(order.getPrice()))*(Integer.parseInt(item.getQuantity()));
        Locale locale=new Locale("en","US");
        NumberFormat numberFormat=NumberFormat.getCurrencyInstance(locale);*/

        //listData=new Database(cart).getCarts(Common.commonUser.getPhone());

        //Calculando el precio total

        holder.txt_price.setText(numberFormat.format(price));

        holder.txt_cart_name.setText(listData.get(position).getProductName());

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public Order getItem(int position){
        return listData.get(position);
    }
    public void removeItem(int position){
        listData.remove(position);
        notifyItemRemoved(position);
    }
    public void restoreItem(Order item,int position){
        listData.add(position,item);
        notifyItemInserted(position);
    }
}
