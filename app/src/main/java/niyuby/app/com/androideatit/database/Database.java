package niyuby.app.com.androideatit.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

import niyuby.app.com.androideatit.model.Order;

public class Database extends SQLiteAssetHelper{
    private static final String DB_NAME="EatItDB.db";
    private static final int DB_VER=1;

    public Database(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    public List<Order>getCarts(String s){
    SQLiteDatabase database=getReadableDatabase();
        SQLiteQueryBuilder queryBuilder=new SQLiteQueryBuilder();

        String[] sqlSelect={"ID","ProductName","ProductId","Quantity","Price","Discount"};
        String sqlTable="OrderDetail";

        queryBuilder.setTables(sqlTable);
        Cursor c=queryBuilder.query(database,sqlSelect,null,null,null,null,null);

        final  List<Order> result=new ArrayList<>();

        if(c.moveToFirst()){
            do{
                result.add(new Order(
                        c.getInt(c.getColumnIndex("ID")),
                        c.getString(c.getColumnIndex("ProductId")),
                        c.getString(c.getColumnIndex("ProductName")),
                        c.getString(c.getColumnIndex("Quantity")),
                        c.getString(c.getColumnIndex("Price")),
                        c.getString(c.getColumnIndex("Discount"))));

            }while (c.moveToNext());
        }
        return result;
    }

    public void addToCart(Order order){
        SQLiteDatabase database=getReadableDatabase();
        String query=String.format
("INSERT INTO OrderDetail(ProductId,ProductName,Quantity,Price,Discount) VALUES('%s','%s','%s','%s','%s');",
        order.getProductId(),
        order.getProductName(),
        order.getQuantity(),
        order.getPrice(),
        order.getDiscount());
        database.execSQL(query);

    }
    public void updateCart(Order order){
        SQLiteDatabase database=getReadableDatabase();
        String query=String.format
                ("UPDATE OrderDetail SET Quantity= %s WHERE ID = %d",
                        order.getQuantity(),
                        order.getID());
        database.execSQL(query);

    }
    public void removeFromCart(String productId) {
        SQLiteDatabase database=getReadableDatabase();
        String query=String.format("DELETE FROM OrderDetail WHERE ProductId='%s'",productId);

        database.execSQL(query);
    }
    public void cleanCart(){
        SQLiteDatabase database=getReadableDatabase();
        String query=String.format("DELETE FROM OrderDetail");

        database.execSQL(query);

    }

    public Database removeFromFavorites(String productId) {
        SQLiteDatabase database=getReadableDatabase();
        String query=String.format("DELETE FROM OrderDetail "+productId);

        database.execSQL(query);
        return null;
    }



}
