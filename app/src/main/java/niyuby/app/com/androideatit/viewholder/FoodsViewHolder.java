package niyuby.app.com.androideatit.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import niyuby.app.com.androideatit.R;
import niyuby.app.com.androideatit.interfacemodel.ItemClickListener;

public class FoodsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
    public TextView textView;
    public ImageView imageView;
    private ItemClickListener itemClickListener;
    public FoodsViewHolder(View itemView) {
        super(itemView);
        textView=(TextView)itemView.findViewById(R.id.txt_menu_list);
        imageView=(ImageView)itemView.findViewById(R.id.img_menu_list);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
    }
}
