package niyuby.app.com.androideatit.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import niyuby.app.com.androideatit.R;

public class MainActivity extends AppCompatActivity {
  TextView textView;
  ImageView imageView;
  Button button,button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView=(TextView)findViewById(R.id.txt_slogan);
        imageView=(ImageView)findViewById(R.id.img_logo);
        button=(Button)findViewById(R.id.btn_signup);
        button2=(Button)findViewById(R.id.btn_signin);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signUp=new Intent(MainActivity.this,SignUpActivity.class);
                startActivity(signUp);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signIn=new Intent(MainActivity.this,SignInActivity.class);
                startActivity(signIn);
            }
        });
    }

}
