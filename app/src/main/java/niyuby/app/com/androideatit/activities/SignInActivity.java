package niyuby.app.com.androideatit.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import niyuby.app.com.androideatit.R;
import niyuby.app.com.androideatit.common.Common;
import niyuby.app.com.androideatit.model.User;

public class SignInActivity extends AppCompatActivity {
EditText editText1,editText2;
Button button;
TextView txt_forgot_pass;
     FirebaseDatabase firebaseDatabase;
     DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        button=(Button)findViewById(R.id.btn_signIn);
        editText1=(MaterialEditText)findViewById(R.id.edittxt_phone);
        editText2=(MaterialEditText)findViewById(R.id.edittxt_pass);
        txt_forgot_pass=(TextView)findViewById(R.id.txt_forgot_pass);
        txt_forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogForgot();
            }
        });
//Comenzando con Firebase

firebaseDatabase=FirebaseDatabase.getInstance();
 databaseReference=firebaseDatabase.getReference("User");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressDialog=new ProgressDialog(SignInActivity.this);
                progressDialog.setMessage("Please waiting...");
                progressDialog.show();
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {// cambiando el metodo
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Verficando que existe el usuario...
                        if(dataSnapshot.child(editText1.getText().toString()).exists()){
                        //Obteniendo la informacion...
                        progressDialog.dismiss();
                        User user=dataSnapshot.child(editText1.getText().toString()).getValue(User.class);
                            user.setPhone(editText1.getText().toString());

                        if(user.getPassword().equals(editText2.getText().toString())){
                            //Toast.makeText(SignInActivity.this,"Sign In is successfully!",Toast.LENGTH_LONG).show();
                            Intent homeScreen=new Intent(SignInActivity.this, HomeActivity.class);
                            Common.commonUser=user;
                            startActivity(homeScreen);
                            finish();
                            databaseReference.removeEventListener(this);

                        }else {
                            Toast.makeText(SignInActivity.this,"Wrong password",Toast.LENGTH_LONG).show();
                        }
                        }else{
                            progressDialog.dismiss();
                            Toast.makeText(SignInActivity.this,"User not exist",Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    private void showDialogForgot() {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
        alertDialog.setTitle("Forgot password").setMessage("Enter your secure code: ");

        LayoutInflater inflater=this.getLayoutInflater();
        View forgot_view=inflater.inflate(R.layout.forgot_pass_layout,null);
        alertDialog.setView(forgot_view);

        final MaterialEditText editText2=(MaterialEditText)forgot_view.findViewById(R.id.edittxt_phone);
        final MaterialEditText editText3=(MaterialEditText)forgot_view.findViewById(R.id.edittxt_secure_code);

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                       User user=dataSnapshot.child(editText2.getText().toString()).getValue(User.class);
                       if(user.getSecureCode().equals(editText3.getText().toString()))
                           Toast.makeText(SignInActivity.this,"Your Password"+user.getPassword(),Toast.LENGTH_LONG).show();
                       else
                           Toast.makeText(SignInActivity.this,"Wrong secure code!",Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }
}
