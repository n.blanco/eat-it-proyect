package niyuby.app.com.androideatit.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import niyuby.app.com.androideatit.R;
import niyuby.app.com.androideatit.interfacemodel.ItemClickListener;
import niyuby.app.com.androideatit.model.Foods;
import niyuby.app.com.androideatit.viewholder.FoodsViewHolder;

public class FoodListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    String categoryId="";
    FirebaseRecyclerAdapter<Foods,FoodsViewHolder> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);
        //Empezando con Firebase
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference("Foods");

        //Cargando el menu
        recyclerView=(RecyclerView)findViewById(R.id.recycler_list);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(FoodListActivity.this);
        recyclerView.setLayoutManager(layoutManager);

        //Obteniendo el intent
        if(getIntent()!=null){
           categoryId =getIntent().getStringExtra("CategoryId");
           if (!categoryId.isEmpty()&&categoryId!=null){
               loadListFood(categoryId);
           }

        }
    }

    private void loadListFood(String categoryId) {
        adapter=new FirebaseRecyclerAdapter<Foods, FoodsViewHolder>(Foods.class,
                R.layout.food_item,FoodsViewHolder.class,
                databaseReference.orderByChild("menuId").equalTo(categoryId)) {
            @Override
            protected void populateViewHolder(FoodsViewHolder viewHolder, Foods model, int position) {
                viewHolder.textView.setText(model.getName());
                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.imageView);
                final Foods foods=model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent foodDetail= new Intent(FoodListActivity.this, FoodDetailActivity.class);
                        foodDetail.putExtra("FoodId",adapter.getRef(position).getKey());
                        startActivity(foodDetail);

                             }
                });
            }
        };
        recyclerView.setAdapter(adapter);
    }
}
